const form = document.getElementById("form") as HTMLFormElement;
const username = document.getElementById("username") as HTMLInputElement;
const email = document.getElementById("email") as HTMLInputElement;
const password = document.getElementById("password") as HTMLInputElement;
const passwordConfirm = document.getElementById(
  "password-confirm",
) as HTMLInputElement;

form.addEventListener("submit", (e) => {
  e.preventDefault();

  const elements = [username, email, password, passwordConfirm];
  resetSuccessState(elements);

  checkRequired(elements);
  checkLength([username], 3, 15);
  checkEmail([email]);

  if (password.value !== passwordConfirm.value) {
    setError(passwordConfirm, "Password is different");
  }
  checkLength([password, passwordConfirm], 6, 25);
});

const resetSuccessState = (elements: HTMLInputElement[]) => {
  elements.forEach((element) => {
    setSuccess(element);
  });
};

const checkRequired = (elements: HTMLInputElement[]) => {
  elements.forEach((element) => {
    if (element.value.trim() === "") {
      setError(element, `${getFieldName(element)} is required`);
    }
  });
};

const checkLength = (
  elements: HTMLInputElement[],
  min: number,
  max: number,
) => {
  elements.forEach((element) => {
    if (element.value.length < min) {
      setError(
        element,
        `${getFieldName(element)} must be at least ${min} characters`,
      );
    } else if (element.value.length >= max) {
      setError(
        element,
        `${getFieldName(element)} must be less than ${max} characters`,
      );
    }
  });
};

const checkEmail = (elements: HTMLInputElement[]) => {
  elements.forEach((element) => {
    const re =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!re.test(String(element.value).toLowerCase())) {
      setError(email, "Email is invalid");
    }
  });
};

const getFieldName = (element: HTMLElement) => {
  if (element.id === "password-confirm") {
    return "Password";
  }
  return `${element.id[0].toUpperCase()}${element.id.slice(1)}`;
};

const setError = (element: HTMLElement, message: string) => {
  const formControl = element.parentElement;
  if (formControl.classList.contains("error")) {
    // Don't override previous error with a new one
    return;
  }

  formControl.classList.remove("success");
  formControl.classList.add("error");
  const small = formControl.querySelector("small");
  small.innerText = message;
};

const setSuccess = (element: HTMLElement) => {
  const formControl = element.parentElement;
  formControl.classList.remove("error");
  formControl.classList.add("success");
};
