# Form Validator

This repository implement the code from the tutorial https://www.udemy.com/course/web-projects-with-vanilla-javascript/.

The goal is to start from what is explained in the tutorial, a simple vanilla javascript project, and then use it as a base to learn more advanced technologies such as:

- NodeJS
- Webpack, ESbuild
- Typescript
- React, VueJS, Angular

At the end the project will not be "vanilla" anymore !

## Typescript Setup

`index.html` is loading `index.js`. The following setup install tools required to compile `index.js` from `index.ts`.

Initialize the project as a npm project (note: we can used `pnpm` or `yarn` instead if we want):

```bash
npm init
```

Install typescript as a development dependency:

```bash
npm install -D typescript
```

Once this is done, `package.json`, `package-lock.json` and `node_modules` are created. `node_modules` should be added to `.gitignore` to avoid pushing locally installed packages to the remote git repository:

```bash
echo "node_modules" >> .gitignore
```

The `typescript` package gives us access to the `tsc` compiler, which can be used that way:

```bash
npx tsc index.ts --sourcemap # Build index.js from index.ts
```

The `--sourcemap` option enables the generation of `index.js.map` that is used by the VSCode debugger so we can debug directly from the typescript source code.

We can also run the compiler in watch mode so we can edit our `index.ts` file and have it recompiled automatically:

```bash
npx tsc index.ts --watch --sourcemap # Build index.js from index.ts and watch for change, run this in a secondary terminal
```

To ease the use of these commands, we add the `build` and `dev` scripts in `package.json`:

```json
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "build": "tsc index.ts --sourcemap",
    "dev": "tsc index.ts --watch --sourcemap"
  },
```

Once everything has been setup that way, a new developer for this project only need to clone the repository and run `npm install` to have all installed in its local repository.

Since `index.js` is now generated from `index.ts`, there is no need to commit and push it anymore. We remove it with `git rm` and add it to `.gitignore` as well as the source map:

```bash
git rm index.js
echo "index.js" >> .gitignore
echo "index.js.map" >> .gitignore
```

## Developer Workflow

The following workflow is for VSCode with the Live Server extension installed.

1. Start typecript compilet in watch mode with `npm run dev` in a new VSCode terminal
2. Start the debugger with F5. Two configurations are provided, one for Edge and one for Chrome.
3. Start the live server from the VSCode status bar (or press `F1` then `Live Server: Open with Live Server`)

This order is important to ensure the debugger run the application from its beginning. If you start with the live server then you will miss some instruction and might not be able to trigger early breakpoints.

If at some point you notice that the debugger seems disconnected, shut down both the debugger and the live server before restarting them in the correct order.

## Future Evolutions

### Building / Bundling

The goal here is to remain quite simple in the tooling and workflow, starting from the basics of just having a typescript compiler.

A good evolution would be to use a bundler like `webpack`, or a more recent one like `esbuild`, to automate building and bundling the complete source code (to put in a `./src` subfolder) into a single package under `./build` that could be deployed on a remove server.

Here are some links to reach that goal:

- [Faster than webpack: JavaScript bundling with esbuild - LogRocket Blog](https://blog.logrocket.com/fast-javascript-bundling-with-esbuild/) ⏳ 4m
- [Comparing the New Generation of Build Tools | CSS-Tricks ](https://css-tricks.com/comparing-the-new-generation-of-build-tools/) ⏳ 30m
- [Getting to know Webpack in 2020 - Beginner friendly introduction | Part 1 | thedeployguy](https://thedeployguy.com/2020-06-07-getting-to-know-webpack/#annotations:bKPY7DWPEeuihAu_UpS71A)
- [Getting to know Webpack - How to add Typescript and CSS support | Part 2 | thedeployguy](https://thedeployguy.com/2020-06-14-adding-typescript-to-web/#annotations:U5jDjDZVEeuum7dW5ablDQ)

### Linting

Setting up `eslint` and `tslint` to enforce some good practice:

- [Setup .eslint, .prettier, .editorconfig, .jsconfig in react apps](https://mepritam.dev/references/setup-eslint-prettier-editorconfig-jsconfig-in-react-apps/) ⏳ 1m
- [Setup TypeScript Standard Linting With TSLint – maevadevs – I speak JS, Python, C#. I live in the Cloud.](https://maevadevs.github.io/setup-typescript-standard-linting-with-tslint/)
- [Configuring TSLint](https://palantir.github.io/tslint/usage/configuration/) ⏳ 4m

`prettier` is already partially configured for this project, but the following link gives many more details:

- [How to configure Prettier and VSCode | Better world by better software](https://glebbahmutov.com/blog/configure-prettier-in-vscode/#settings) ⏳ 17m

### Testing

Trying to add unit tests with [`jest`](https://jestjs.io/) to this project could be a good training on doing TDD in a simple project. Here are some resources:

- [Unit testing with vanilla JavaScript: The very basics - DEV Community 👩‍💻👨‍💻](https://dev.to/aurelkurtula/unit-testing-with-vanilla-javascript-the-very-basics-7jm) ⏳ 10m
- [How to Unit Test HTML and Vanilla JavaScript Without a UI Framework - DEV Community 👩‍💻👨‍💻](https://dev.to/thawkin3/how-to-unit-test-html-and-vanilla-javascript-without-a-ui-framework-4io) ⏳ 6m
- [Things I learned after writing tests for JS and HTML page - DEV Community 👩‍💻👨‍💻](https://dev.to/snowleo208/things-i-learned-after-writing-tests-for-js-and-html-page-4lja) ⏳ 3m
- [Testing your JavaScript Code — TDD. | by Tolu Adesina | Medium](https://toluadesina.medium.com/testing-your-javascript-code-tdd-e3ab872c461d)
- [Efficient TDD with Karma and Webpack | Bamboo Engineering](https://bambooengineering.io/efficient-tdd-with-karma-and-webpack/) ⏳ 3m
